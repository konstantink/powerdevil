# Translation for powerdevilactivitiesconfig.po to Euskara/Basque (eu).
# Copyright (C) 2011-2018, Free Software Foundation.
# Copyright (C) 2019-2022, This file is copyright:
# This file is distributed under the same license as the powerdevil package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2011, 2014, 2018, 2019, 2020, 2022.
# Hizkuntza Politikarako Sailburuordetza <hizpol@ej-gv.es>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-06 02:04+0000\n"
"PO-Revision-Date: 2022-08-20 15:29+0200\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Iñigo Salvador Azurmendi,Hizkuntza Politikarako Sailburuordetza"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xalba@ni.eus,hizpol@ej-gv.es"

#: activitypage.cpp:59
#, kde-format
msgid ""
"The activity service is running with bare functionalities.\n"
"Names and icons of the activities might not be available."
msgstr ""
"Jarduera-zerbitzua oinarrizko funtzionaltasunekin exekutatzen ari da.\n"
"Baliteke jarduera-izenak eta -ikonoak erabilgarri ez egotea."

#: activitypage.cpp:132
#, kde-format
msgid ""
"The activity service is not running.\n"
"It is necessary to have the activity manager running to configure activity-"
"specific power management behavior."
msgstr ""
"Jarduera-zerbitzua ez da exekutatzen ari.\n"
"Jarduera-kudeatzailea exekutatzen egotea beharrezkoa da, jardueren "
"berariazko energia-kudeaketa jokabidea konfiguratzeko."

#: activitypage.cpp:227
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Ematen du energia kudeatzeko zerbitzua ez dagoela martxan."

#. i18n: ectx: property (text), widget (QRadioButton, noSettingsRadio)
#: activityWidget.ui:19
#, kde-format
msgid "Do not use special settings"
msgstr "Ez erabili ezarpen berezirik"

#. i18n: ectx: property (text), widget (QRadioButton, specialBehaviorRadio)
#: activityWidget.ui:29
#, kde-format
msgid "Define a special behavior"
msgstr "Definitu jokabide berezi bat"

#. i18n: ectx: property (text), widget (QCheckBox, noShutdownScreenBox)
#: activityWidget.ui:41
#, kde-format
msgid "Never turn off the screen"
msgstr "Ez itzali pantaila inoiz"

#. i18n: ectx: property (text), widget (QCheckBox, noShutdownPCBox)
#: activityWidget.ui:48
#, kde-format
msgid "Never shut down the computer or let it go to sleep"
msgstr "Sekula ez itzali ordenagailua ez eta lokartzen utzi ere"

#~ msgctxt "Suspend to RAM"
#~ msgid "Sleep"
#~ msgstr "Egin lo"

#~ msgid "Hibernate"
#~ msgstr "Hibernatu"

#~ msgid "Shut down"
#~ msgstr "Itzali"

#~ msgid "Always"
#~ msgstr "Beti"

#~ msgid "after"
#~ msgstr "honen ondoren:"

#~ msgid " min"
#~ msgstr " min"

#~ msgid "PC running on AC power"
#~ msgstr "PCa korronte alternoarekin ibiltzen ari da"

#~ msgid "PC running on battery power"
#~ msgstr "PCa bateria-energiarekin ibiltzen ari da"

#~ msgid "PC running on low battery"
#~ msgstr "PCa bateria eskasarekin ibiltzen ari da"

#~ msgctxt "This is meant to be: Act like activity %1"
#~ msgid "Activity \"%1\""
#~ msgstr "\"%1\" jarduera"

#~ msgid "Act like"
#~ msgstr "Jokatu honela:"

#~ msgid "Use separate settings"
#~ msgstr "Erabili ezarpen bereiziak"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Itxura denez, energia kudeatzeko zerbitzua ez da exekutatzen ari.\n"
#~ "\"Abioa eta itzaltzea\" aukeran abiarazi edo antolatuz konpon daiteke "
#~ "hori."

#~ msgid "Suspend"
#~ msgstr "Eseki"

#~ msgid "Never suspend or shutdown the computer"
#~ msgstr "Ez eseki edo itzali inoiz ordenagailua"

#~ msgid "Activities Power Management Configuration"
#~ msgstr "Jardueretako energia kudeatzeko konfigurazioa"

#~ msgid "A per-activity configurator of KDE Power Management System"
#~ msgstr "KDEren energia kudeatzeko sistemaren jarduerarako konfiguratzailea"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(c), 2010 Dario Freddi"

#~ msgid ""
#~ "From this module, you can fine tune power management settings for each of "
#~ "your activities."
#~ msgstr ""
#~ "Modulu honetan, energia kudeatzeko ezarpenak jarduera bakoitzerako doi "
#~ "ditzakezu."

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "Arduraduna"
