# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2011, 2012, 2013, 2020, 2021.
# Cristian Oneț <onet.cristian@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-06 02:04+0000\n"
"PO-Revision-Date: 2021-07-25 14:00+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 19.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sergiu Bivol"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sergiu@cip.md"

#: GeneralPage.cpp:95
#, kde-format
msgid "Do nothing"
msgstr "Nu fă nimic"

#: GeneralPage.cpp:98
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Adormire"

#: GeneralPage.cpp:103
#, kde-format
msgid "Hibernate"
msgstr "Hibernare"

#: GeneralPage.cpp:106
#, kde-format
msgid "Shut down"
msgstr "Deconectare"

#: GeneralPage.cpp:255
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Se pare că serviciul de gestiune a alimentării nu se execută."

#. i18n: ectx: property (text), widget (QLabel, batteryLevelsLabel)
#: generalPage.ui:22
#, kde-format
msgid "<b>Battery Levels                     </b>"
msgstr "<b>Niveluri acumulator             </b>"

#. i18n: ectx: property (text), widget (QLabel, lowLabel)
#: generalPage.ui:29
#, kde-format
msgid "&Low level:"
msgstr "Nive&l scăzut:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, lowSpin)
#: generalPage.ui:39
#, kde-format
msgid "Low battery level"
msgstr "Nivel scăzut al acumulatorului"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, lowSpin)
#: generalPage.ui:42
#, kde-format
msgid "Battery will be considered low when it reaches this level"
msgstr ""
"Acumulatorul va fi considerat ca fiind la un nivel scăzut când atinge acest "
"nivel"

#. i18n: ectx: property (suffix), widget (QSpinBox, lowSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, criticalSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, lowPeripheralSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStartThresholdSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStopThresholdSpin)
#: generalPage.ui:45 generalPage.ui:71 generalPage.ui:114 generalPage.ui:167
#: generalPage.ui:230
#, no-c-format, kde-format
msgid "%"
msgstr "%"

#. i18n: ectx: property (text), widget (QLabel, criticalLabel)
#: generalPage.ui:55
#, kde-format
msgid "&Critical level:"
msgstr "Nivel &critic:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, criticalSpin)
#: generalPage.ui:65
#, kde-format
msgid "Critical battery level"
msgstr "Nivel critic al acumulatorului"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, criticalSpin)
#: generalPage.ui:68
#, kde-format
msgid "Battery will be considered critical when it reaches this level"
msgstr ""
"Acumulatorul va fi considerat ca fiind la un nivel critic când atinge acest "
"nivel"

#. i18n: ectx: property (text), widget (QLabel, BatteryCriticalLabel)
#: generalPage.ui:81
#, kde-format
msgid "A&t critical level:"
msgstr "La nivelul cri&tic:"

#. i18n: ectx: property (text), widget (QLabel, lowPeripheralLabel)
#: generalPage.ui:107
#, kde-format
msgid "Low level for peripheral devices:"
msgstr "Nivel scăzut pentru dispozitive periferice:"

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdLabel)
#: generalPage.ui:130
#, kde-format
msgid "Charge Limit"
msgstr "Limită de încărcare"

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdExplanation)
#: generalPage.ui:137
#, no-c-format, kde-format
msgid ""
"<html><head/><body><p>Keeping the battery charged 100% over a prolonged "
"period of time may accelerate deterioration of battery health. By limiting "
"the maximum battery charge you can help extend the battery lifespan.</p></"
"body></html>"
msgstr ""
"<html><head/><body><p>Păstrarea acumulatorului încărcat la 100% pe o durată "
"lungă de timp poate accelera deteriorarea sănătății acestuia. Prin limitarea "
"încărcării maxime a acumulatorului, puteți contribui la prelungirea duratei "
"sale de viață.</p></body></html>"

#. i18n: ectx: property (text), widget (KMessageWidget, chargeStopThresholdMessage)
#: generalPage.ui:147
#, kde-format
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""
"S-ar putea să fie necesară deconectarea și reconectarea sursei de alimentare "
"pentru în reîncepe încărcarea acumulatorului."

#. i18n: ectx: property (text), widget (QLabel, chargeStartThresholdLabel)
#: generalPage.ui:157
#, kde-format
msgid "Start charging only once below:"
msgstr "Începe încărcarea odată ce e sub:"

#. i18n: ectx: property (specialValueText), widget (QSpinBox, chargeStartThresholdSpin)
#: generalPage.ui:164
#, kde-format
msgid "Always charge when plugged in"
msgstr "Încarcă întotdeauna când e conectat la priză"

#. i18n: ectx: property (text), widget (QLabel, pausePlayersLabel)
#: generalPage.ui:177
#, kde-format
msgid "Pause media players when suspending:"
msgstr "Întrerupe redarea multimedia la suspendare:"

#. i18n: ectx: property (text), widget (QCheckBox, pausePlayersCheckBox)
#: generalPage.ui:184
#, kde-format
msgid "Enabled"
msgstr "Activat"

#. i18n: ectx: property (text), widget (QPushButton, notificationsButton)
#: generalPage.ui:203
#, kde-format
msgid "Configure Notifications…"
msgstr "Configurează notificările…"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: generalPage.ui:216
#, kde-format
msgid "Other Settings"
msgstr "Alte configurări"

#. i18n: ectx: property (text), widget (QLabel, chargeStopThresholdLabel)
#: generalPage.ui:223
#, kde-format
msgid "Stop charging at:"
msgstr "Oprește încărcarea la:"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Se pare că serviciul de gestionare a alimentării nu este în execuție.\n"
#~ "Această problemă se poate rezolva prin pronirea serviciului sau "
#~ "planificarea lui în \"Pornire și oprire\""

#~ msgid "<b>Events</b>"
#~ msgstr "<b>Evenimente</b>"

#~ msgid ""
#~ "When this option is selected, applications will not be allowed to inhibit "
#~ "sleep when the lid is closed"
#~ msgstr ""
#~ "Când această opțiune e aleasă, aplicațiile nu vor avea voie să inhibe "
#~ "adormirea la închiderea capacului"

#~ msgid "Never prevent an action on lid close"
#~ msgstr "Nu bloca niciodată acțiunile la închiderea capacului"

#~ msgid "Locks screen when waking up from suspension"
#~ msgstr "Blochează ecranul la trezirea din suspendare"

#~ msgid "You will be asked for a password when resuming from sleep state"
#~ msgstr "Vi se va cere o parolă la revenirea din starea de adormire"

#, fuzzy
#~| msgid "Lock screen on resume"
#~ msgid "Loc&k screen on resume"
#~ msgstr "Blochează ecranul la reluare"

#~ msgid "Battery is at low level at"
#~ msgstr "Acumulatorul este la nivel scăzut la"

#~ msgid "When battery is at critical level"
#~ msgstr "Când acumulatorul e la nivelul critic"

#~ msgid "Global Power Management Configuration"
#~ msgstr "Configurație globală pentru gestiunea alimentării"

#~ msgid ""
#~ "A global power management configurator for KDE Power Management System"
#~ msgstr ""
#~ "Un configurator pentru gestiunea globală a alimentării pentru sistemul "
#~ "KDE de gestiune a alimentării"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(c), 2010 Dario Freddi"

#~ msgid ""
#~ "From this module, you can configure the main Power Management daemon, "
#~ "assign profiles to states, and do some advanced fine tuning on battery "
#~ "handling"
#~ msgstr ""
#~ "În acest modul puteți configura demonul principal de gestiune a "
#~ "alimentării, să atribuiți profiluri la stări și să efectuați reglaje fine "
#~ "asupra gestiunii acumulatorului"

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "Responsabil"

#~ msgid "Settings and Profile"
#~ msgstr "Configurări și profil"

#~ msgid ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#~ "weight:600;\">Profile Assignment</span></p></body></html>"
#~ msgstr ""
#~ "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
#~ "REC-html40/strict.dtd\">\n"
#~ "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/"
#~ "css\">\n"
#~ "p, li { white-space: pre-wrap; }\n"
#~ "</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; "
#~ "font-weight:400; font-style:normal;\">\n"
#~ "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
#~ "right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
#~ "weight:600;\">Atribuire profil</span></p></body></html>"

#~ msgid "When AC Adaptor is plugged in"
#~ msgstr "Când adaptorul de alimentare este atașat"

#~ msgid "When AC Adaptor is unplugged"
#~ msgstr "Când adaptorul de alimentare este detașat"

#~ msgid "When battery is at warning level"
#~ msgstr "Când acumulatorul a atins nivelul de avertizare"

#~ msgid "When battery remaining is critical"
#~ msgstr "Când acumulatorul rămas este critic"

#~ msgid "Battery is at warning level at"
#~ msgstr "Acumulatorul este la nivel de avertizare la"

#~ msgid "Warning battery level"
#~ msgstr "Nivel de avertizare al acumulatorului"

#~ msgid ""
#~ "Battery will be considered at warning level when it reaches this level"
#~ msgstr ""
#~ "Acumulatorul va fi considerat ca fiind la un nivel de avertizare când "
#~ "atinge acest nivel"
