# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the powerdevil package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: powerdevil\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-06 02:04+0000\n"
"PO-Revision-Date: 2022-11-16 08:32+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Temuri Doghonadze"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Temuri.doghonadze@gmail.com"

#: GeneralPage.cpp:95
#, kde-format
msgid "Do nothing"
msgstr "არაფრის გაკეთება"

#: GeneralPage.cpp:98
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "ძილი"

#: GeneralPage.cpp:103
#, kde-format
msgid "Hibernate"
msgstr "პროგრამული ძილი"

#: GeneralPage.cpp:106
#, kde-format
msgid "Shut down"
msgstr "გამორთვა"

#: GeneralPage.cpp:255
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "კვების მართვის სერვისი მგონი გაშვებული არაა."

#. i18n: ectx: property (text), widget (QLabel, batteryLevelsLabel)
#: generalPage.ui:22
#, kde-format
msgid "<b>Battery Levels                     </b>"
msgstr "<b>ელემენტის დონეები                     </b>"

#. i18n: ectx: property (text), widget (QLabel, lowLabel)
#: generalPage.ui:29
#, kde-format
msgid "&Low level:"
msgstr "&მუხტის დაბალი დონე:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, lowSpin)
#: generalPage.ui:39
#, kde-format
msgid "Low battery level"
msgstr "ელემენტის მუხტის დაბალი დონე"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, lowSpin)
#: generalPage.ui:42
#, kde-format
msgid "Battery will be considered low when it reaches this level"
msgstr "ელემენტი ჩაითვლება განმუხტულად, როცა ამ დონეს მიაღწევს"

#. i18n: ectx: property (suffix), widget (QSpinBox, lowSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, criticalSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, lowPeripheralSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStartThresholdSpin)
#. i18n: ectx: property (suffix), widget (QSpinBox, chargeStopThresholdSpin)
#: generalPage.ui:45 generalPage.ui:71 generalPage.ui:114 generalPage.ui:167
#: generalPage.ui:230
#, no-c-format, kde-format
msgid "%"
msgstr "%"

#. i18n: ectx: property (text), widget (QLabel, criticalLabel)
#: generalPage.ui:55
#, kde-format
msgid "&Critical level:"
msgstr "&მუხტის კრიტიკული დონე:"

#. i18n: ectx: property (toolTip), widget (QSpinBox, criticalSpin)
#: generalPage.ui:65
#, kde-format
msgid "Critical battery level"
msgstr "ელემენტის მუხტის კრიტიკული დონე"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, criticalSpin)
#: generalPage.ui:68
#, kde-format
msgid "Battery will be considered critical when it reaches this level"
msgstr "ელემენტი ჩაირთვება კრიტიკულად განმუხტულად, როცა ამ დონეს მიაღწევს"

#. i18n: ectx: property (text), widget (QLabel, BatteryCriticalLabel)
#: generalPage.ui:81
#, kde-format
msgid "A&t critical level:"
msgstr "&კრიტიკული დონისას:"

#. i18n: ectx: property (text), widget (QLabel, lowPeripheralLabel)
#: generalPage.ui:107
#, kde-format
msgid "Low level for peripheral devices:"
msgstr "მუხტის დაბალი დონე პერიფერიული მოწყობილობებისათვის:"

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdLabel)
#: generalPage.ui:130
#, kde-format
msgid "Charge Limit"
msgstr "დატენვის ლიმიტი"

#. i18n: ectx: property (text), widget (QLabel, batteryThresholdExplanation)
#: generalPage.ui:137
#, no-c-format, kde-format
msgid ""
"<html><head/><body><p>Keeping the battery charged 100% over a prolonged "
"period of time may accelerate deterioration of battery health. By limiting "
"the maximum battery charge you can help extend the battery lifespan.</p></"
"body></html>"
msgstr ""
"<html><head/><body><p>ელემენტის დიდი ხნის განმავლობაში 100% მუხტით ქონისას "
"მისი წარმადობა შეიძლება საგრძნობლად დაეცეს. ელემენტის მაქსიმალური მუხტის "
"შეზღუდვით თქვენ ელემენტის სიცოცხლის გახანგრძლივება შეგიძლიათ.</p></body></"
"html>"

#. i18n: ectx: property (text), widget (KMessageWidget, chargeStopThresholdMessage)
#: generalPage.ui:147
#, kde-format
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""
"თქვენ შეიძლება მოგიწიოთ ენერგიის წყაროს გათიშვა და ხელახლა დაკავშირება, რომ "
"კვლავ დაიწყოთ ელემენტის დამუხტვა."

#. i18n: ectx: property (text), widget (QLabel, chargeStartThresholdLabel)
#: generalPage.ui:157
#, kde-format
msgid "Start charging only once below:"
msgstr "დატენვა დაიწყება, როცა მუხტი მითითებულ დონეზე დაბლა ჩამოვა:"

#. i18n: ectx: property (specialValueText), widget (QSpinBox, chargeStartThresholdSpin)
#: generalPage.ui:164
#, kde-format
msgid "Always charge when plugged in"
msgstr "ყოველთვის დატენვა, როცა შეერთებულია"

#. i18n: ectx: property (text), widget (QLabel, pausePlayersLabel)
#: generalPage.ui:177
#, kde-format
msgid "Pause media players when suspending:"
msgstr "ძილისთვის მომზადებისას მედიის დამკვირებელის შეჩერება:"

#. i18n: ectx: property (text), widget (QCheckBox, pausePlayersCheckBox)
#: generalPage.ui:184
#, kde-format
msgid "Enabled"
msgstr "ჩართულია"

#. i18n: ectx: property (text), widget (QPushButton, notificationsButton)
#: generalPage.ui:203
#, kde-format
msgid "Configure Notifications…"
msgstr "შეტყობინებების მორგება…"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: generalPage.ui:216
#, kde-format
msgid "Other Settings"
msgstr "სხვა პარამეტრები"

#. i18n: ectx: property (text), widget (QLabel, chargeStopThresholdLabel)
#: generalPage.ui:223
#, kde-format
msgid "Stop charging at:"
msgstr "დატენვის შეჩერება მუხტის დონისას:"
